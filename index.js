const capitalize = require('./src/capitalize');
const { validateCPF, maskCPF, unmaskCPF, formatCPF, isValidCPF } = require('./src/cpf');
const { validateCNPJ, maskCNPJ, unmaskCNPJ, isValidCNPJ, formatCNPJ } = require('./src/cnpj');
const { getCCNumberInfo, validateCCExpiry, validateCCNumber } = require('./src/credit-card');
const { maskPhone, unmaskPhone, isValidPhone, validatePhone } = require('./src/phone');
const { isValidEmail } = require('./src/email');
const { isValidBoleto, formatBoleto } = require('./src/boleto');
const initials = require('./src/initials');
const urlFormat = require('./src/url-format');

module.exports = {
  capitalize,
  validateCPF,
  maskCPF,
  unmaskCPF,
  validateCNPJ,
  maskCNPJ,
  unmaskCNPJ,
  validateCCExpiry,
  validateCCNumber,
  getCCNumberInfo,
  initials,
  urlFormat,
  maskPhone,
  unmaskPhone,
  formatCPF,
  isValidCPF,
  isValidCNPJ,
  formatCNPJ,
  validatePhone,
  isValidPhone,
  isValidEmail,
  isValidBoleto,
  formatBoleto,
};

/**
 * Returns a capitalized string
 *
 * @func
 * @param {string} val The value to be capitalized
 * @return {string}
 */

function capitalize(val) {
  const arr = val
    .trim()
    .replace(/\s+/g, ' ')
    .toLowerCase()
    .split(' ');
  const newArr = [];
  arr.forEach((word) => {
    newArr.push(word[0].toUpperCase() + word.slice(1));
  });
  return newArr.join(' ');
}

module.exports = capitalize;

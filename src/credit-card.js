const valid = require('card-validator');

/**
 * Returns a boolean validating if a cc number is valid or not
 *
 * @func
 * @param {string} val The cc number to be validated
 * @return {boolean}
 */
function validateCCNumber(val) {
  return valid.number(val).isValid;
}

/**
 * Returns a boolean validating if a cc expiration date is valid or not
 *
 * @func
 * @param {string} val The cc expiration date to be validated
 * @return {boolean}
 */
function validateCCExpiry(val) {
  return valid.expirationDate(val).isValid;
}

/**
 * Returns a object with a credit card number information
 *
 * @func
 * @param {string} val The cc number to be validated
 * @return {object}
 */
function getCCNumberInfo(val) {
  return valid.number(val);
}

module.exports = {
  validateCCNumber,
  validateCCExpiry,
  getCCNumberInfo,
};

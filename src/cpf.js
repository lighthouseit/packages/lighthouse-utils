const { isValidCPF, formatCPF, isValidPIS } = require('@brazilian-utils/brazilian-utils');

/**
 * Returns a boolean validating if a CPF is valid or not
 * Copyright https://www.geradorcpf.com/javascript-validar-cpf.htm
 *
 * @func
 * @param {string} val The value to be validated
 * @return {boolean}
 */

function validateCPF(val) {
  const cpf = val.replace(/[^\d]+/g, '');
  if (cpf === '') return false;

  // Deletes known invalid CPFs
  if (
    cpf.length !== 11
    || cpf === '00000000000'
    || cpf === '11111111111'
    || cpf === '22222222222'
    || cpf === '33333333333'
    || cpf === '44444444444'
    || cpf === '55555555555'
    || cpf === '66666666666'
    || cpf === '77777777777'
    || cpf === '88888888888'
    || cpf === '99999999999'
  ) return false;

  // Validate 1º digit
  let add = 0;
  for (let i = 0; i < 9; i++) add += parseInt(cpf.charAt(i), 10) * (10 - i);
  let rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) rev = 0;
  if (rev !== parseInt(cpf.charAt(9), 10)) return false;

  // Valida 2o digito
  add = 0;
  for (let i = 0; i < 10; i++) add += parseInt(cpf.charAt(i), 10) * (11 - i);
  rev = 11 - (add % 11);
  if (rev === 10 || rev === 11) rev = 0;
  if (rev !== parseInt(cpf.charAt(10), 10)) return false;
  return true;
}

const maskCPF = cpf => cpf.replace(/([0-9]{3})([0-9]{3})([0-9]{3})([0-9]{2})/gm, '$1.$2.$3-$4');

const unmaskCPF = cpf => cpf.replace(/([.|-])/gm, '');

module.exports = {
  validateCPF,
  maskCPF,
  unmaskCPF,
  isValidCPF,
  formatCPF,
  isValidPIS,
};

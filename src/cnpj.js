const { isValidCNPJ, formatCNPJ } = require('@brazilian-utils/brazilian-utils');

/**
 * Returns a boolean validating if a CNPJ is valid or not
 * Copyright https://www.geradorcnpj.com/javascript-validar-cnpj.htm
 *
 * @func
 * @param {string} val The value to be validated
 * @return {boolean}
 */

function validateCNPJ(val) {
  const cnpj = val.replace(/[^\d]+/g, '');

  if (cnpj === '') return false;

  if (cnpj.length !== 14) return false;

  if (cnpj === '00000000000000'
    || cnpj === '11111111111111'
    || cnpj === '22222222222222'
    || cnpj === '33333333333333'
    || cnpj === '44444444444444'
    || cnpj === '55555555555555'
    || cnpj === '66666666666666'
    || cnpj === '77777777777777'
    || cnpj === '88888888888888'
    || cnpj === '99999999999999') return false;

  let lengthCNPJ = cnpj.length - 2;
  let numbers = cnpj.substring(0, lengthCNPJ);
  const digits = cnpj.substring(lengthCNPJ);
  let sum = 0;
  let position = lengthCNPJ - 7;
  for (let i = lengthCNPJ; i >= 1; i--) {
    sum += numbers.charAt(lengthCNPJ - i) * position--;
    if (position < 2) position = 9;
  }
  let result = (sum % 11) < 2 ? 0 : 11 - (sum % 11);
  if (result.toString() !== digits.charAt(0)) return false;

  lengthCNPJ += 1;
  numbers = cnpj.substring(0, lengthCNPJ);
  sum = 0;
  position = lengthCNPJ - 7;
  for (let i = lengthCNPJ; i >= 1; i--) {
    sum += numbers.charAt(lengthCNPJ - i) * position--;
    if (position < 2) position = 9;
  }

  result = (sum % 11) < 2 ? 0 : 11 - (sum % 11);
  if (result.toString() !== digits.charAt(1)) return false;

  return true;
}

const maskCNPJ = cnpj => cnpj
  .replace(/\D/g, '')
  .replace(/(\d{2})(\d)/, '$1.$2')
  .replace(/(\d{3})(\d)/, '$1.$2')
  .replace(/(\d{3})(\d)/, '$1/$2')
  .replace(/(\d{4})(\d)/, '$1-$2');

const unmaskCNPJ = cnpj => cnpj.replace(/[.|/|-]/gm, '');

module.exports = {
  validateCNPJ,
  maskCNPJ,
  unmaskCNPJ,
  isValidCNPJ,
  formatCNPJ,
};

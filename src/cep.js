const { isValidCEP, formatCEP } = require('@brazilian-utils/brazilian-utils');

module.exports = {
  isValidCEP,
  formatCEP,
};

const { isValidEmail } = require('@brazilian-utils/brazilian-utils');

module.exports = {
  isValidEmail,
};

const initials = require('../src/initials');

describe('initials', () => {
  it('returns the initials for a string', () => {
    const str = 'Donald Silveira';
    const result = initials(str);

    expect(result).toEqual('DS');
  });
});

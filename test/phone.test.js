const { validatePhone } = require('../src/phone');

describe('validatePhone', () => {
  const invalid = [
    '',
    '456',
    '51899262245',
    'dsaa',
    '999898878',
  ];

  const valid = [
    '(51)998784454',
    '(51) 998784454',
    '(51) 99878-4454',
    '51998784454',
  ];

  it('returns false for invalid phones', () => {
    invalid.forEach((v) => {
      const result = validatePhone(v);
      expect(result).toBeFalsy();
    });
  });

  it('returns false for valid phones', () => {
    valid.forEach((v) => {
      const result = validatePhone(v);
      expect(result).toBeTruthy();
    });
  });
});

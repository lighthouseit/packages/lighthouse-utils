const { validateCPF } = require('../src/cpf');

describe('validateCPF', () => {
  const invalid = [
    '00000000000',
    '11111111111',
    '22222222222',
    '33333333333',
    '44444444444',
    '55555555555',
    '66666666666',
    '77777777777',
    '88888888888',
    '99999999999',
    '10203040506',
  ];

  const valid = ['11922749036', '87208853002', '85892348008', '39253394005'];

  it('returns false for invalid CPFs', () => {
    invalid.forEach((v) => {
      const result = validateCPF(v);
      expect(result).toBeFalsy();
    });
  });

  it('returns false for valid CPFs', () => {
    valid.forEach((v) => {
      const result = validateCPF(v);
      expect(result).toBeTruthy();
    });
  });
});
